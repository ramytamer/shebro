window.$ = window.jQuery = require('jquery');

require('./semantic.js');

$(document).ready(function() {
  $('.ui.accordion').accordion();
});

window._ = require('lodash');
window.Vue = require('vue');

Vue.component('main-app', require('./components/MainApp.vue'));
Vue.component('tool', require('./components/Tool.vue'));
Vue.component('runner', require('./components/Runner.vue'));

window.tools = require('./tools.js').default;
window.inputs = require('./inputs.js').default;

console.info('tools', window.tools);

new Vue({
  el: '#app'
});
