export default [
  {
    electrical: { battery: 0.80, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000, bendingMoment: 1000000, },
      2: {radialForce: 100000, bendingMoment: 1000000, },
      3: {radialForce: 10000, bendingMoment: 1000000, },
    },
  },
  {
    electrical: { battery: 0.80, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000, bendingMoment: 1000000, },
      2: {radialForce: 100000, bendingMoment: 1000000, },
      3: {radialForce: 10000, bendingMoment: 1000000, },
    },
  },
  {
    electrical: { battery: 0.81, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000, bendingMoment: 1000000, },
      2: {radialForce: 100000, bendingMoment: 1000000, },
      3: {radialForce: 10000, bendingMoment: 1000000, },
    },
  },
  {
    electrical: { battery: 0.82, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000, bendingMoment: 1000000, },
      2: {radialForce: 100000, bendingMoment: 1000000, },
      3: {radialForce: 10000, bendingMoment: 1000000, },
    },
  },
  {
    electrical: { battery: 0.86, generatedVoltage: 0 }, // OVER CHARGING ERROR !! MUST LAST 6 secs
    mechanical: {
      1: {radialForce: 10000, bendingMoment: 1000000, },
      2: {radialForce: 100000, bendingMoment: 1000000, },
      3: {radialForce: 10000, bendingMoment: 1000000, },
    },
  },
  {
    electrical: { battery: 0.86, generatedVoltage: 0 }, // OVER CHARGING ERROR !! MUST LAST 6 secs
    mechanical: {
      1: {radialForce: 10000, bendingMoment: 1000000, },
      2: {radialForce: 100000, bendingMoment: 1000000, },
      3: {radialForce: 10000, bendingMoment: 1000000, },
    },
  },
  {
    electrical: { battery: 0.86, generatedVoltage: 0 }, // OVER CHARGING ERROR !! MUST LAST 6 secs
    mechanical: {
      1: {radialForce: 10000, bendingMoment: 1000000, },
      2: {radialForce: 100000, bendingMoment: 1000000, },
      3: {radialForce: 10000, bendingMoment: 1000000, },
    },
  },
  {
    electrical: { battery: 0.87, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000, bendingMoment: 1000000, },
      2: {radialForce: 100000, bendingMoment: 1000000, },
      3: {radialForce: 10000, bendingMoment: 1000000, },
    },
  },
  {
    electrical: { battery: 0.88, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000, bendingMoment: 1000000, },
      2: {radialForce: 100000, bendingMoment: 1000000, },
      3: {radialForce: 10000, bendingMoment: 1000000, },
    },
  },
  {
    electrical: { battery: 0.89, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000, bendingMoment: 1000000, },
      2: {radialForce: 100000, bendingMoment: 1000000, },
      3: {radialForce: 10000, bendingMoment: 1000000, },
    },
  },
  {
    electrical: { battery: 0.85, generatedVoltage: 0 }, // OVER DISCHARGING ERROR !! MUST LAST 6 secs
    mechanical: {
      1: {radialForce: 10000, bendingMoment: 1000000, },
      2: {radialForce: 100000, bendingMoment: 1000000, },
      3: {radialForce: 10000, bendingMoment: 1000000, },
    },
  },
  {
    electrical: { battery: 0.85, generatedVoltage: 0 }, // OVER DISCHARGING ERROR !! MUST LAST 6 secs
    mechanical: {
      1: {radialForce: 10000, bendingMoment: 1000000, },
      2: {radialForce: 100000, bendingMoment: 1000000, },
      3: {radialForce: 10000, bendingMoment: 1000000, },
    },
  },
  {
    electrical: { battery: 0.85, generatedVoltage: 0 }, // OVER DISCHARGING ERROR !! MUST LAST 6 secs
    mechanical: {
      1: {radialForce: 10000, bendingMoment: 1000000, },
      2: {radialForce: 100000, bendingMoment: 1000000, },
      3: {radialForce: 10000, bendingMoment: 1000000, },
    },
  },
  {
    electrical: { battery: 0.86, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000, bendingMoment: 1000000, },
      2: {radialForce: 100000, bendingMoment: 1000000, },
      3: {radialForce: 10000, bendingMoment: 1000000, },
    },
  },

  // ========================================================
  // END OF BATTERY TRIALS ONLY
  // ========================================================
  // START OPERATING MODE
  // ========================================================
  
  {
    electrical: { battery: 0.87, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000, bendingMoment: 1000000, },
      2: {radialForce: 100000, bendingMoment: 1000000, },
      3: {radialForce: 10000, bendingMoment: 1000000, },
    },
  },
  {
    electrical: { battery: 0.89, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000, bendingMoment: 1000000, },
      2: {radialForce: 100000, bendingMoment: 1000000, },
      3: {radialForce: 10000, bendingMoment: 1000000, },
    },
  },
  {
    electrical: { battery: 0.91, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000, bendingMoment: 1000000, },
      2: {radialForce: 100000, bendingMoment: 1000000, },
      3: {radialForce: 10000, bendingMoment: 1000000, },
    },
  },
  {
    electrical: { battery: 0.92, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000, bendingMoment: 1000000, },
      2: {radialForce: 100000, bendingMoment: 1000000, },
      3: {radialForce: 10000, bendingMoment: 1000000, },
    },
  },
  {
    electrical: { battery: 0.92, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000, bendingMoment: 1000000, },
      2: {radialForce: 100000, bendingMoment: 1000000, },
      3: {radialForce: 10000, bendingMoment: 1000000, },
    },
  },
  {
    electrical: { battery: 0.92, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000, bendingMoment: 1000000, },
      2: {radialForce: 100000, bendingMoment: 1000000, },
      3: {radialForce: 10000, bendingMoment: 1000000, },
    },
  },

  // ========================================================
  // END OPERATING MODE
  // ========================================================
  // START MECHANICAL MODE
  // ========================================================

  {
    electrical: { battery: 0.93, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000 - 355, bendingMoment: 1000000, },
      2: {radialForce: 100000, bendingMoment: 1000000 - 367, },
      3: {radialForce: 10000 - 345, bendingMoment: 1000000 - 345, },
    },
  },
  {
    electrical: { battery: 0.94, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000 - 343, bendingMoment: 1000000, },
      2: {radialForce: 100000 - 321, bendingMoment: 1000000 - 367, },
      3: {radialForce: 10000 - 345, bendingMoment: 1000000 - 345, },
    },
  },
  {
    electrical: { battery: 0.95, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000 - 343, bendingMoment: 1000000 + 343, },
      2: {radialForce: 100000 - 321, bendingMoment: 1000000 - 355, },
      3: {radialForce: 10000 - 367, bendingMoment: 1000000 - 345, },
    },
  },
  {
    electrical: { battery: 0.96, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000 - 343, bendingMoment: 1000000 + 367, },
      2: {radialForce: 100000 - 321, bendingMoment: 1000000 - 355, },
      3: {radialForce: 10000 - 345, bendingMoment: 1000000 - 343, },
    },
  },
  {
    electrical: { battery: 0.97, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000 - 321, bendingMoment: 1000000 + 355, },
      2: {radialForce: 100000 - 355, bendingMoment: 1000000 - 345, },
      3: {radialForce: 10000 - 343, bendingMoment: 1000000 - 321, },
    },
  },
  {
    electrical: { battery: 0.98, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000 - 355, bendingMoment: 1000000 - 345, },
      2: {radialForce: 100000 + 345, bendingMoment: 1000000 - 343, },
      3: {radialForce: 10000 - 321, bendingMoment: 1000000 - 355, },
    },
  },
  {
    electrical: { battery: 0.99, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000 - 375, bendingMoment: 1000000 - 323, },
      2: {radialForce: 100000 + 367, bendingMoment: 1000000 - 378, },
      3: {radialForce: 10000 - 345, bendingMoment: 1000000 - 365, },
    },
  },
  {
    electrical: { battery: 1.0, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000 - 321, bendingMoment: 1000000 - 343, },
      2: {radialForce: 100000 + 345, bendingMoment: 1000000 - 367, },
      3: {radialForce: 10000 - 355, bendingMoment: 1000000 - 345, },
    },
  },


  // ========================================================
  // END MECHANICAL MODE
  // ========================================================
  // START NORMAL MODE
  // ========================================================

  {
    electrical: { battery: 0.99, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000 - 343, bendingMoment: 1000000 - 321, },
      2: {radialForce: 100000 - 355, bendingMoment: 1000000 - 367, },
      3: {radialForce: 10000 - 345, bendingMoment: 1000000 - 343, },
    },
  },

  {
    electrical: { battery: 0.95, generatedVoltage: 0 },
    mechanical: {
      1: {radialForce: 10000 - 321, bendingMoment: 1000000 - 355, },
      2: {radialForce: 100000 - 367, bendingMoment: 1000000 - 345, },
      3: {radialForce: 10000 - 343, bendingMoment: 1000000 - 321, },
    },
  },
  {
    electrical: { battery: 0.95, generatedVoltage: 0 },
    mechanical: {
      1: {radialForce: 10000 - 355, bendingMoment: 1000000 - 367, },
      2: {radialForce: 100000 + 367, bendingMoment: 1000000 - 345, },
      3: {radialForce: 10000 - 345, bendingMoment: 1000000 - 321, },
    },
  },
  {
    electrical: { battery: 0.95, generatedVoltage: 0 },
    mechanical: {
      1: {radialForce: 10000 - 355, bendingMoment: 1000000 - 367, },
      2: {radialForce: 100000 - 345, bendingMoment: 1000000 - 345, },
      3: {radialForce: 10000 - 321, bendingMoment: 1000000 + 345, },
    },
  },

  {
    electrical: { battery: 0.94, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000 - 355, bendingMoment: 1000000 - 367, },
      2: {radialForce: 100000 - 345, bendingMoment: 1000000 - 345, },
      3: {radialForce: 10000 - 321, bendingMoment: 1000000 - 355, },
    },
  },
  {
    electrical: { battery: 0.95, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000 - 367, bendingMoment: 1000000 - 345, },
      2: {radialForce: 100000 - 345, bendingMoment: 1000000 - 343, },
      3: {radialForce: 10000 - 321, bendingMoment: 1000000 - 355, },
    },
  },
  {
    electrical: { battery: 0.96, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000 - 367, bendingMoment: 1000000 - 345, },
      2: {radialForce: 100000 - 345, bendingMoment: 1000000 - 343, },
      3: {radialForce: 10000 + 367, bendingMoment: 1000000 - 321, },
    },
  },
  {
    electrical: { battery: 0.97, generatedVoltage: 48 },
    mechanical: {
      1: {radialForce: 10000 - 355, bendingMoment: 1000000 - 367, },
      2: {radialForce: 100000 - 345, bendingMoment: 1000000 - 345, },
      3: {radialForce: 10000 - 321, bendingMoment: 1000000 - 355, },
    },
  },

  // ===================================
  // FINAL ERRORS
  // ===================================

  {
    electrical: { battery: 0.94, generatedVoltage: 0 },
    mechanical: {
      1: {radialForce: 10000 + 345, bendingMoment: 1000000 - 367, },
      2: {radialForce: 100000 - 345, bendingMoment: 1000000 - 345, },
      3: {radialForce: 10000 - 343, bendingMoment: 1000000 + 367, },
    },
  },
  {
    electrical: { battery: 0.94, generatedVoltage: 0 },
    mechanical: {
      1: {radialForce: 10000 + 345, bendingMoment: 1000000 - 321, },
      2: {radialForce: 100000 - 355, bendingMoment: 1000000 - 345, },
      3: {radialForce: 10000 - 345, bendingMoment: 1000000 + 345, },
    },
  },
  {
    electrical: { battery: 0.94, generatedVoltage: 0 },
    mechanical: {
      1: {radialForce: 10000 + 345, bendingMoment: 1000000 - 343, },
      2: {radialForce: 100000 - 321, bendingMoment: 1000000 - 355, },
      3: {radialForce: 10000 - 367, bendingMoment: 1000000 + 343, },
    },
  },
  {
    electrical: { battery: 0.94, generatedVoltage: 0 },
    mechanical: {
      1: {radialForce: 10000 + 343, bendingMoment: 1000000 - 345, },
      2: {radialForce: 100000 - 345, bendingMoment: 1000000 - 343, },
      3: {radialForce: 10000 - 321, bendingMoment: 1000000 + 343, },
    },
  },
];
