export default [
  {
    id: 1,
    parameters: [
      {placeholder: 'Inner diameter', value: 3.5, unit: 'in', times: 1, },
      {placeholder: 'Outer diameter', value: 8, unit: 'in', times: 1, },
      {placeholder: 'Length', value: 2.5, unit: 'm', times: 1, },
      {placeholder: 'ρ (density)', value: 8, unit: 'kg/m<sup>3</sup>', times: 1000, },
      {placeholder: 'Poisson Ratio', value: 0.275, uni: '%', times:1, },
      {placeholder: 'Modulus Of Elasticity', value: 152, unit: 'GPa', times: 1, },
      {placeholder: 'Modulus Of Rigidity', value: 310, unit: 'MPa', times: 1, },
      {placeholder: 'Compressible strength', value: 310, unit: 'MPa', times: 1, },
      {placeholder: 'Tensile strength', value: 620, unit: 'MPa', times: 1, },
      {placeholder: 'Shear Modulus', value: 82, unit: 'GPa', times: 1, },
      {placeholder: 'Young\'s Modulus', value: 205, unit: 'GPa', times: 1, },
      {placeholder: 'Maximum radial force', value: 10000, unit: 'N', times: 1, },
      {placeholder: 'Maximum bending moment', value: 1000000, unit: 'N/m', times: 1, },
    ]
  },
  {
    id: 2,
    parameters: [
      {placeholder: 'Inner diameter', value: 3.3, unit: 'in', times: 1, },
      {placeholder: 'Outer diameter', value: 8, unit: 'in', times: 1, },
      {placeholder: 'Length', value: 2.7, unit: 'm', times: 1, },
      {placeholder: 'ρ (density)', value: 9, unit: 'kg/m<sup>3</sup>', times: 1000, },
      {placeholder: 'Poisson Ratio', value: 0.365, unit: '', times: 1, },
      {placeholder: 'Modulus Of Elasticity', value: 162, unit: 'GPa', times: 1, },
      {placeholder: 'Modulus Of Rigidity', value: 320, unit: 'MPa', times: 1, },
      {placeholder: 'Compressible strength', value: 300, unit: 'MPa', times: 1, },
      {placeholder: 'Tensile strength', value: 730, unit: 'MPa', times: 1, },
      {placeholder: 'Shear Modulus', value: 83, unit: 'GPa', times: 1, },
      {placeholder: 'Young\'s Modulus', value: 215, unit: 'GPa', times: 1, },
      {placeholder: 'Maximum radial force', value: 100000, unit: 'N', times: 1, },
      {placeholder: 'Maximum bending moment', value: 1000000, unit: 'N/m', times: 1, },
    ]
  },
  {
    id: 3,
    parameters: [
      {placeholder: 'Inner diameter', value: 3.1, unit: 'in', times: 1, },
      {placeholder: 'Outer diameter', value: 8, unit: 'in', times: 1, },
      {placeholder: 'Length', value: 2.4, unit: 'm', times: 1, },
      {placeholder: 'ρ (density)', value: 8, unit: 'kg/m<sup>3</sup>', times: 1000, },
      {placeholder: 'Poisson Ratio', value: 0.305, unit: '', times: 1, },
      {placeholder: 'Modulus Of Elasticity', value: 148, unit: 'GPa', times: 1, },
      {placeholder: 'Modulus Of Rigidity', value: 339, unit: 'MPa', times: 1, },
      {placeholder: 'Compressible strength', value: 294, unit: 'MPa', times: 1, },
      {placeholder: 'Tensile strength', value: 680, unit: 'MPa', times: 1, },
      {placeholder: 'Shear Modulus', value: 79, unit: 'GPa', times: 1, },
      {placeholder: 'Young\'s Modulus', value: 205, unit: 'GPa', times: 1, },
      {placeholder: 'Maximum radial force', value: 10000, unit: 'N', times: 1, },
      {placeholder: 'Maximum bending moment', value: 1000000, unit: 'N/m', times: 1, },
    ]
  },
];
